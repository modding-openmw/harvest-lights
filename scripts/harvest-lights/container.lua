local core = require('openmw.core')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local types = require('openmw.types')
local common = require("scripts.harvest-lights.common")
local harvested
local lights = {}

local function notifyShroom()
    local now = core.getGameTime()
    harvested = now
    local nearbyLights = {}
    for _, thing in pairs(nearby.items) do
        if types.Light.objectIsInstance(thing) then
            table.insert(nearbyLights, thing)
        end
    end
    core.sendGlobalEvent(
        "momw_hl_checkLightsForContainer",
        {
            nearbyLights = nearbyLights,
            theContainer = self,
            lights = lights,
            now = now
        }
    )
end

local function saveLight(data)
    table.insert(lights, data.light)
end

local function unHarvest()
    harvested = nil
    lights = {}
end

local function onActive()
    core.sendGlobalEvent(
        "momw_hl_onContainerActive",
        {
            theContainer = self,
            lights = lights,
            harvested = harvested
        }
    )
end

local function onLoad(data)
    if data.scriptVersion == 1 then
        harvested = data.harvested[self.id]
    else
        harvested = data.harvested
    end
    lights = data.lights
end

local function onSave()
    return {
        harvested = harvested,
        lights = lights,
        scriptVersion = common.scriptVersion
    }
end

return {
    engineHandlers = {
        onActive = onActive,
        onLoad = onLoad,
        onSave = onSave
    },
    eventHandlers = {
        momw_hl_notifyShroom = notifyShroom,
        momw_hl_saveLight = saveLight,
        momw_hl_unHarvest = unHarvest
    }
}
